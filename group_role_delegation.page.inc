<?php

/**
 * @file
 * Contains group_role_delegation.page.inc.
 *
 * Page callback for Group role delegation entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Group role delegation templates.
 *
 * Default template: group_role_delegation.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_group_role_delegation(array &$variables) {
  // Fetch GroupRoleDelegation Entity Object.
  $group_role_delegation = $variables['elements']['#group_role_delegation'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
