<?php

namespace Drupal\group_role_delegation\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Group role delegation entities.
 *
 * @ingroup group_role_delegation
 */
class GroupRoleDelegationDeleteForm extends ContentEntityDeleteForm {


}
