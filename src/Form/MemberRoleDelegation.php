<?php

/**
 * @file
 * Contains \Drupal\group_role_delegation\Form\MemberRoleDelegation.
 */

namespace Drupal\group_role_delegation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity;
use Drupal\group_role_delegation\Controller\GroupRoleDelegationCronController;

/**
 * Class MemberRoleDelegation.
 */
class MemberRoleDelegation extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'member_role_delegation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $group = null, $group_content = null) {

    //Check any existing delegation
    $query = \Drupal::entityQuery('group_role_delegation');
    $query->condition('field_status.value', 'Active', '=');
    $query->condition('field_group_id.value', $group, '=');
    $query->condition('field_group_member_id.value', $group_content, '=');
    $entity_ids = $query->execute();

    //Return empty form if any delegation exists
    if(!empty($entity_ids))
      return $form;

    //Get Group Content entity
    $group_content_entity = \Drupal::service('entity_type.manager')
      ->getStorage('group_content')
      ->load($group_content);

    $membership = new \Drupal\group\GroupMembership($group_content_entity);

    //Returns the group roles for the membership.
    foreach($membership->getRoles() as $role){
      $member_roles[$role->id()] = $role->label();
    }

    foreach($membership->getGroup()->getGroupType()->getRoles(false) as $role){
      $group_roles[$role->id()] = $role->label();
    }

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#required' => TRUE,
      '#value' => $membership->getUser()->getUsername(),
      '#attributes' => array('readonly' => 'readonly'),
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#value' => $membership->getUser()->getEmail(),
      '#attributes' => array('readonly' => 'readonly'),
    ];

    $form['group'] = [
      '#type' => 'hidden',
      '#value' => $group,
    ];

    $form['group_content'] = [
      '#type' => 'hidden',
      '#value' => $group_content,
    ];

    $form['member_roles'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group roles'),
      '#required' => TRUE,
      '#value' => json_encode($member_roles),
      '#attributes' => array('readonly' => 'readonly'),
    ];

    $date_format = 'Y-m-d';
    $time_format = 'H:i';
    $form['start_date'] = [
      '#type' => 'date',
      '#title' => t('Start Date'),
      '#date_date_format' => $date_format,
      '#date_time_format' => $time_format,
    ];

    $form['end_date'] = [
      '#type' => 'date',
      '#title' => t('End Date'),
      '#required' => TRUE,
      '#date_date_format' => $date_format,
      '#date_time_format' => $time_format,
    ];

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => $group_roles,
      '#value' => $member_roles
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $start_date = $form_state->getValue('start_date');
    $end_date = $form_state->getValue('end_date');
    if (!empty($start_date)) {
      $start_date = strtotime($start_date);
      $end_date = strtotime($end_date);
      if ($start_date > $end_date) {
        $form_state->setErrorByName('start_date', t('End date should be greater than Start Date.'));
      }
      if ($start_date == $end_date) {
        $form_state->setErrorByName('start_date', t('Should be Start and End dates are Different.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    //Get submitted data.
    $member_roles = $form_state->getValue('member_roles');
    $start_date = $form_state->getValue('start_date');
    if (empty($start_date)) {
      $start_date = date('Y-m-d');
    }
    $end_date = $form_state->getValue('end_date');
    $roles = \Drupal::request()->request->get('roles');
    $group = $form_state->getValue('group');
    $group_content = $form_state->getValue('group_content');

    // Member informations.
    $username = $form_state->getValue('username');
    $member = user_load_by_name($username);
    $existing_system_roles = $new_system_roles = $member->getRoles();
    $user_id = $member->id();
    // System Roles.
    $system_roles = [
      'creator' => 'content_creator',
      'reviewer' => 'content_reviewer',
      'approver' => 'content_approver',
      'department-head' => 'department_head',
      'coordinator' => 'content_coordinator',
      'manager' => 'user_manager',
    ];

    $assigned_roles = array();
    foreach ($roles as $key => $role) {
      if($role) {
        $assigned_roles[$key] =  $role;

        //Assign new system roles to member.
        $group_role = end(explode("_", $role));
        if (!empty($system_roles[$group_role])) {
          if (!in_array($system_roles[$group_role], $new_system_roles)) {
            $new_system_roles[] = $system_roles[$group_role];
          }
        }
      }
    }

    // Save Role Delegation Entity.
    $entity = entity_create('group_role_delegation', array('type' => 'group_role_delegation'));
    $entity->field_assigned_roles = json_encode($assigned_roles);
    $entity->field_current_roles = $member_roles;
    $member_info = [
      'username' => $username, //username of delegated member.
      'roles' => $existing_system_roles, // Currenr roles of delegated member.
    ];
    $entity->field_current_system_roles = json_encode($member_info);
    $entity->field_delegated_system_roles = json_encode($new_system_roles);
    $entity->field_start_date = $start_date;
    $entity->field_end_date = $end_date;
    $entity->field_group_id = $group;
    $entity->field_group_member_id = $group_content;
    $entity->field_status = 'Pending';
    $entity->name = $email;
    $entity->revision->value = $email;
    $entity->save();

    //Run cron for group role delegation
    GroupRoleDelegationCronController::cronDelegation();
  }

}
