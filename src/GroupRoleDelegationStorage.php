<?php

namespace Drupal\group_role_delegation;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface;

/**
 * Defines the storage handler class for Group role delegation entities.
 *
 * This extends the base storage class, adding required special handling for
 * Group role delegation entities.
 *
 * @ingroup group_role_delegation
 */
class GroupRoleDelegationStorage extends SqlContentEntityStorage implements GroupRoleDelegationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(GroupRoleDelegationInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {group_role_delegation_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {group_role_delegation_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(GroupRoleDelegationInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {group_role_delegation_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('group_role_delegation_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
