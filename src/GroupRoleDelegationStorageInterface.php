<?php

namespace Drupal\group_role_delegation;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface;

/**
 * Defines the storage handler class for Group role delegation entities.
 *
 * This extends the base storage class, adding required special handling for
 * Group role delegation entities.
 *
 * @ingroup group_role_delegation
 */
interface GroupRoleDelegationStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Group role delegation revision IDs for a specific Group role delegation.
   *
   * @param \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface $entity
   *   The Group role delegation entity.
   *
   * @return int[]
   *   Group role delegation revision IDs (in ascending order).
   */
  public function revisionIds(GroupRoleDelegationInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Group role delegation author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Group role delegation revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface $entity
   *   The Group role delegation entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(GroupRoleDelegationInterface $entity);

  /**
   * Unsets the language for all Group role delegation with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
