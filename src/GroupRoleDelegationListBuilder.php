<?php

namespace Drupal\group_role_delegation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Group role delegation entities.
 *
 * @ingroup group_role_delegation
 */
class GroupRoleDelegationListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Group role delegation ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\group_role_delegation\Entity\GroupRoleDelegation */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.group_role_delegation.edit_form',
      ['group_role_delegation' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
