<?php

namespace Drupal\group_role_delegation;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Group role delegation entity.
 *
 * @see \Drupal\group_role_delegation\Entity\GroupRoleDelegation.
 */
class GroupRoleDelegationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished group role delegation entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published group role delegation entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit group role delegation entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete group role delegation entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add group role delegation entities');
  }

}
