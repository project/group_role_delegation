<?php

/**
 * @file
 * Contains Drupal\group_role_delegation\Controller\GroupRoleDelegationCronController.
 */

namespace Drupal\group_role_delegation\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GroupRoleDelegationCronController.
 *
 *  Returns responses for Group role delegation cron routes.
 */
class GroupRoleDelegationCronController extends ControllerBase {

  /**
   * Callback for cron.
   */
  public static function cronDelegation() {
    // Fire the pending schedulers.
    $query = \Drupal::entityQuery('group_role_delegation');
    $query->condition('field_status.value', 'Pending', '=');
    $query->condition('field_start_date.value', date('Y-m-d'), '<=');
    $entity_ids = $query->execute();

    if (!empty($entity_ids)) {

      $role_delegations = \Drupal::entityTypeManager()->getStorage('group_role_delegation')->loadMultiple($entity_ids);

      foreach ($role_delegations as $role_delegation) {
        $group_content = $role_delegation->get('field_group_member_id')->getValue()[0]['value'];
        if (!empty($group_content)) {

          // Assign System roles to user.
          // Username and Current roles assign to variable member_info.
          $member_info = (array) json_decode($role_delegation->get('field_current_system_roles')->getValue()[0]['value']);
          $delegated_system_roles = (array) json_decode($role_delegation->get('field_delegated_system_roles')->getValue()[0]['value']);
          // Member informations.
          $username = $member_info['username'];
          $member = user_load_by_name($username);
          $member->set('roles', $delegated_system_roles);
          // Disable system sync now, will be fixed it later.
          // $member->save();

          // Get Group Content entity.
          $group_content_entity = \Drupal::service('entity_type.manager')
            ->getStorage('group_content')
            ->load($group_content);
          $assigned_roles = (array) json_decode($role_delegation->get('field_assigned_roles')->getValue()[0]['value']);
          // Flush current roles.
          $group_content_entity->group_roles = new \Drupal\Core\Field\EntityReferenceFieldItemList();

          // Add each assigned roles to member.
          foreach ($assigned_roles as $key => $assigned_role) {
            $group_role = \Drupal::service('entity_type.manager')
              ->getStorage('group_role')
              ->load($key);
            $group_content_entity->group_roles->appendItem($group_role);
          }

          $group_content_entity->save();

          // Change scheduler status to Active.
          $role_delegation->field_status = 'Active';
          $role_delegation->save();
        }
      }
    }
    // Deactivate expired schedulers.
    $query = \Drupal::entityQuery('group_role_delegation');
    $query->condition('field_status.value', 'Active', '=');
    $query->condition('field_end_date.value', date('Y-m-d'), '<');
    $entity_ids = $query->execute();

    if (!empty($entity_ids)) {
      $role_delegations = \Drupal::entityTypeManager()->getStorage('group_role_delegation')->loadMultiple($entity_ids);
      foreach ($role_delegations as $role_delegation) {
        $group_content = $role_delegation->get('field_group_member_id')->getValue()[0]['value'];

        if (!empty($group_content)) {

          // Revert System roles to user.
          // Username and Current roles assign to variable member_info.
          $member_info = (array) json_decode($role_delegation->get('field_current_system_roles')->getValue()[0]['value']);
          // Member informations.
          $username = $member_info['username'];
          $revert_system_roles = $member_info['roles'];
          $member = user_load_by_name($username);
          $member->set('roles', $revert_system_roles);
          // Disable system sync now, will be fixed it later.
          // $member->save();

          // Get Group Content entity.
          $group_content_entity = \Drupal::service('entity_type.manager')
            ->getStorage('group_content')
            ->load($group_content);

          $current_roles = (array) json_decode($role_delegation->get('field_current_roles')->getValue()[0]['value']);

          // Flush delegated roles.
          $group_content_entity->group_roles = new \Drupal\Core\Field\EntityReferenceFieldItemList();

          // Revert roles to member.
          foreach ($current_roles as $key => $role) {
            $group_role = \Drupal::service('entity_type.manager')
              ->getStorage('group_role')
              ->load($key);
            $group_content_entity->group_roles->appendItem($group_role);
          }

          $group_content_entity->save();
          // Change scheduler status to Expired.
          $role_delegation->field_status = 'Expired';
          $role_delegation->save();
        }
      }
    }
    drupal_set_message(t('Updated Cron Delegation settings.'), 'status');
    return array(
      '#markup' => t('Delegation Cron executed successfully'),
    );
  }

}
