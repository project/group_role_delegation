<?php

/**
 * @file
 * Contains Drupal\group_role_delegation\Controller\GroupRoleDelegationController.
 */

namespace Drupal\group_role_delegation\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface;

/**
 * Class GroupRoleDelegationController.
 *
 *  Returns responses for Group role delegation routes.
 */
class GroupRoleDelegationController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Group role delegation  revision.
   *
   * @param int $group_role_delegation_revision
   *   The Group role delegation  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($group_role_delegation_revision) {
    $group_role_delegation = $this->entityManager()->getStorage('group_role_delegation')->loadRevision($group_role_delegation_revision);
    $view_builder = $this->entityManager()->getViewBuilder('group_role_delegation');

    return $view_builder->view($group_role_delegation);
  }

  /**
   * Page title callback for a Group role delegation  revision.
   *
   * @param int $group_role_delegation_revision
   *   The Group role delegation  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($group_role_delegation_revision) {
    $group_role_delegation = $this->entityManager()->getStorage('group_role_delegation')->loadRevision($group_role_delegation_revision);
    return $this->t('Revision of %title from %date', ['%title' => $group_role_delegation->label(), '%date' => format_date($group_role_delegation->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Group role delegation .
   *
   * @param \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface $group_role_delegation
   *   A Group role delegation  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(GroupRoleDelegationInterface $group_role_delegation) {
    $account = $this->currentUser();
    $langcode = $group_role_delegation->language()->getId();
    $langname = $group_role_delegation->language()->getName();
    $languages = $group_role_delegation->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $group_role_delegation_storage = $this->entityManager()->getStorage('group_role_delegation');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $group_role_delegation->label()]) : $this->t('Revisions for %title', ['%title' => $group_role_delegation->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all group role delegation revisions") || $account->hasPermission('administer group role delegation entities')));
    $delete_permission = (($account->hasPermission("delete all group role delegation revisions") || $account->hasPermission('administer group role delegation entities')));

    $rows = [];

    $vids = $group_role_delegation_storage->revisionIds($group_role_delegation);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\group_role_delegation\GroupRoleDelegationInterface $revision */
      $revision = $group_role_delegation_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $group_role_delegation->getRevisionId()) {
          $link = $this->l($date, new Url('entity.group_role_delegation.revision', ['group_role_delegation' => $group_role_delegation->id(), 'group_role_delegation_revision' => $vid]));
        }
        else {
          $link = $group_role_delegation->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.group_role_delegation.translation_revert',
                [
                  'group_role_delegation' => $group_role_delegation->id(),
                  'group_role_delegation_revision' => $vid,
                  'langcode' => $langcode,
                ]) :
              Url::fromRoute('entity.group_role_delegation.revision_revert', ['group_role_delegation' => $group_role_delegation->id(), 'group_role_delegation_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.group_role_delegation.revision_delete', ['group_role_delegation' => $group_role_delegation->id(), 'group_role_delegation_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['group_role_delegation_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * @param \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface $group_role_delegation
   *   A Group role delegation  object.
   */
  public function revert(GroupRoleDelegationInterface $group_role_delegation) {
    $group = $group_role_delegation->get('field_group_id')->getValue()[0]['value'];
    $group_content = $group_role_delegation->get('field_group_member_id')->getValue()[0]['value'];
    if (!empty($group_content)) {
      // Revert System roles to user.
      // Username and Current roles assign to variable member_info.
      $member_info = (array) json_decode($group_role_delegation->get('field_current_system_roles')->getValue()[0]['value']);
      // Member informations.
      $username = $member_info['username'];
      $revert_system_roles = $member_info['roles'];
      $member = user_load_by_name($username);
      $member->set('roles', $revert_system_roles);
      $member->save();

      // Get Group Content entity.
      $group_content_entity = \Drupal::service('entity_type.manager')
        ->getStorage('group_content')
        ->load($group_content);

      $current_roles = (array) json_decode($group_role_delegation->get('field_current_roles')->getValue()[0]['value']);

      // Flush delegated roles.
      $group_content_entity->group_roles = new \Drupal\Core\Field\EntityReferenceFieldItemList();

      // Revert roles to member.
      foreach ($current_roles as $key => $role) {
        $group_role = \Drupal::service('entity_type.manager')
          ->getStorage('group_role')
          ->load($key);
        $group_content_entity->group_roles->appendItem($group_role);
      }
      $group_content_entity->save();

      // Change scheduler status to Reverted.
      $group_role_delegation->field_status = 'Reverted';
      $group_role_delegation->save();
      drupal_set_message(t('Delegation has been reverted.'), 'status');
    }
    return $this->redirect(
      'entity.group_content_type.member.role_delegation',
      [
        'group' => $group,
        'group_content' => $group_content,
      ]
    );
  }

}
