<?php

namespace Drupal\group_role_delegation;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for group_role_delegation.
 */
class GroupRoleDelegationTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
