<?php

namespace Drupal\group_role_delegation\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Group role delegation entity.
 *
 * @ingroup group_role_delegation
 *
 * @ContentEntityType(
 *   id = "group_role_delegation",
 *   label = @Translation("Group role delegation"),
 *   handlers = {
 *     "storage" = "Drupal\group_role_delegation\GroupRoleDelegationStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\group_role_delegation\GroupRoleDelegationListBuilder",
 *     "views_data" = "Drupal\group_role_delegation\Entity\GroupRoleDelegationViewsData",
 *     "translation" = "Drupal\group_role_delegation\GroupRoleDelegationTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\group_role_delegation\Form\GroupRoleDelegationForm",
 *       "add" = "Drupal\group_role_delegation\Form\GroupRoleDelegationForm",
 *       "edit" = "Drupal\group_role_delegation\Form\GroupRoleDelegationForm",
 *       "delete" = "Drupal\group_role_delegation\Form\GroupRoleDelegationDeleteForm",
 *     },
 *     "access" = "Drupal\group_role_delegation\GroupRoleDelegationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\group_role_delegation\GroupRoleDelegationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "group_role_delegation",
 *   data_table = "group_role_delegation_field_data",
 *   revision_table = "group_role_delegation_revision",
 *   revision_data_table = "group_role_delegation_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer group role delegation entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/group_role_delegation/{group_role_delegation}",
 *     "add-form" = "/admin/structure/group_role_delegation/add",
 *     "edit-form" = "/admin/structure/group_role_delegation/{group_role_delegation}/edit",
 *     "delete-form" = "/admin/structure/group_role_delegation/{group_role_delegation}/delete",
 *     "version-history" = "/admin/structure/group_role_delegation/{group_role_delegation}/revisions",
 *     "revision" = "/admin/structure/group_role_delegation/{group_role_delegation}/revisions/{group_role_delegation_revision}/view",
 *     "revision_revert" = "/admin/structure/group_role_delegation/{group_role_delegation}/revisions/{group_role_delegation_revision}/revert",
 *     "revision_delete" = "/admin/structure/group_role_delegation/{group_role_delegation}/revisions/{group_role_delegation_revision}/delete",
 *     "translation_revert" = "/admin/structure/group_role_delegation/{group_role_delegation}/revisions/{group_role_delegation_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/group_role_delegation",
 *   },
 *   field_ui_base_route = "group_role_delegation.settings"
 * )
 */
class GroupRoleDelegation extends RevisionableContentEntityBase implements GroupRoleDelegationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the group_role_delegation owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Group role delegation entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Group role delegation entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Group role delegation is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }
  
  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    if ($rel === 'revision_revert' && $this instanceof RevisionableContentEntityBase) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableContentEntityBase) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    return $uri_route_parameters;
  }

}
