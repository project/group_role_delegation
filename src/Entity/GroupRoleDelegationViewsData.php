<?php

namespace Drupal\group_role_delegation\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Group role delegation entities.
 */
class GroupRoleDelegationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
