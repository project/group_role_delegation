<?php

namespace Drupal\group_role_delegation\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Group role delegation entities.
 *
 * @ingroup group_role_delegation
 */
interface GroupRoleDelegationInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Group role delegation name.
   *
   * @return string
   *   Name of the Group role delegation.
   */
  public function getName();

  /**
   * Sets the Group role delegation name.
   *
   * @param string $name
   *   The Group role delegation name.
   *
   * @return \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface
   *   The called Group role delegation entity.
   */
  public function setName($name);

  /**
   * Gets the Group role delegation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Group role delegation.
   */
  public function getCreatedTime();

  /**
   * Sets the Group role delegation creation timestamp.
   *
   * @param int $timestamp
   *   The Group role delegation creation timestamp.
   *
   * @return \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface
   *   The called Group role delegation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Group role delegation published status indicator.
   *
   * Unpublished Group role delegation are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Group role delegation is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Group role delegation.
   *
   * @param bool $published
   *   TRUE to set this Group role delegation to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface
   *   The called Group role delegation entity.
   */
  public function setPublished($published);

  /**
   * Gets the Group role delegation revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Group role delegation revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface
   *   The called Group role delegation entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Group role delegation revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Group role delegation revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\group_role_delegation\Entity\GroupRoleDelegationInterface
   *   The called Group role delegation entity.
   */
  public function setRevisionUserId($uid);

}
